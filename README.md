# IMDB
By Amin Malekpour
---
# Technology, Tool and framework
These technologies, tools and frameworks have been used in the project:
1. Java 8
2. Spring Boot (Web, Test)
3. Maven
4. SpringFox (swagger2, swagger-ui)
6. Lombok
7. MongoDB
---

# Data Layer
Considering the assignment's requirements, I used mongoDB and in some cases indexed some filed for faster
response. Also, used spring data for manipulating and searching Database.
---

# Project structure
I have used Maven as the build tool.
Source codes are located under the “imdb/src/main/java” folder.
I used Project Lombok which is a java library that automatically plugs into your IDE and generating getter, setters, hashcode, equals and etc for your POJOs. please if you are using IDE like 
Intellij Idea consider installing lombok plugin for your IDE (https://plugins.jetbrains.com/plugin/6317-lombok-plugin)
---

# Document
REST API documentation is provided via “Swagger-UI”.
Use this link:   http://localhost:8080/swagger-ui.html
---

# Test
I have written some useful test classes under the “imdb/src/test/java” folder. I used spring-boot-test
and JUnit to implement unit and integration tests. These tests were implemented by using facilities like
TestRestTemplate. If I had more time, I would implement more unit tests for controller, business and data layer.
---

# How to run the application
Use the following commands to build and run the application:
1. mvn clean install
2. mvn spring-boot:run
---

# How to fill Database:
Before running application: 
   Install and run mongoDb, use or edit properties in application.properties file.

This endpoint is used when you want to fill Database. This endpoint reads data from '*.tsv' 
files and fills the Database:
1. POST ../api/imdb/coincidence

Before using the above endpoint: 
1. It is better to drop the database or tables.
2. Download data from: https://datasets.imdbws.com
3. Extract files
4. Define the directory of the extracted files in 'application.properties' with key 'file.directory'.
5. The endpoint fill all mongo collections. For more information see the table below:

   ----------------------------------------------------------------------------------------
   No. Status   Collection   File            Download
   ----------------------------------------------------------------------------------------
   1.  required  name         name.tsv       https://datasets.imdbws.com/name.basics.tsv.gz
   2.  required  principal    principal.tsv  https://datasets.imdbws.com/title.principals.tsv.gz
   3.  required  basic        basic.tsv      https://datasets.imdbws.com/title.basics.tsv.gz
   4.  optional  crew         crew.tsv       https://datasets.imdbws.com/title.crew.tsv.gz
   5.  optional  episode      name.tsv       https://datasets.imdbws.com/title.episode.tsv.gz
   6.  optional  aka          aka.tsv        https://datasets.imdbws.com/title.akas.tsv.gzprincipal
   7.  optional  ratings      ratings.tsv    https://datasets.imdbws.com/title.ratings.tsv.gz

6. You can only download and use required (see above table) 
   so you can omit others by commenting the lines:
      FillDatabaseServiceImpl.java:56-59
      FillDatabaseServiceImpl.java:295-309
---
# Other endpoints:   
The assignment asked use actor/actress/person name for getting data from endpoints.
The better way is using personId instead of the name because it is unique but name may not be unique.
1. GET ../api/imdb/person/{name}
2. GET ../api/imdb/typecasting/{personId}
3. GET ../api/imdb/coincidence?personId1={id1}&personId2={id2}
4. GET ../api/imdb/degrees/{personId}

For finding personId by a name(case sensitive) use endpoint 1 from the above list.
You can use the endpoint 1 for entering the actor/actress/person name and get his/her personId.
Finally use the personId for using endpoints 2, 3 and 4.

