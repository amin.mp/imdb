package com.amin.imdb.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

/**
 * @author <a href="amin.malekpour@hotmail.com">Amin Malekpour</a>
 * @version 1, 12 Feb 2019
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Document(collection = "episode")
public class Episode extends Entity {
    @Field
    private String parentTconst;
    @Field
    private Integer seasonNumber;
    @Field
    private Integer episodeNumber;
}
