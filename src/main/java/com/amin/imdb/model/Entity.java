package com.amin.imdb.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;

/**
 * @author <a href="amin.malekpour@hotmail.com">Amin Malekpour</a>
 * @version 1, 12 Feb 2019
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public abstract class Entity {
    @Id
    private String id;
}
