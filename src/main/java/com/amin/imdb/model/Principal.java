package com.amin.imdb.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

/**
 * @author <a href="amin.malekpour@hotmail.com">Amin Malekpour</a>
 * @version 1, 12 Feb 2019
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Document(collection = "principal")
public class Principal extends Entity {
    @Field
    @Indexed
    private String tconst;
    @Field
    private Integer ordering;
    @Field
    @Indexed
    private String nconst;
    @Field
    private String category;
    @Field
    private String job;
    @Field
    private String characters;
}
