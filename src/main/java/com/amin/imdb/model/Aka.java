package com.amin.imdb.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import java.util.ArrayList;
import java.util.List;

/**
 * @author <a href="amin.malekpour@hotmail.com">Amin Malekpour</a>
 * @version 1, 12 Feb 2019
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Document(collection = "aka")
public class Aka extends Entity {
    @Field
    private String titleId;
    @Field
    private Integer ordering;
    @Field
    private String title;
    @Field
    private String region;
    @Field
    private String language;
    @Field
    private List<String> type = new ArrayList<>();
    @Field
    private List<String> attribute = new ArrayList<>();
    @Field
    private Boolean isOriginalTitle;
}
