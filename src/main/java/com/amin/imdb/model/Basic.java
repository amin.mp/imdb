package com.amin.imdb.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import java.util.ArrayList;
import java.util.List;

/**
 * @author <a href="amin.malekpour@hotmail.com">Amin Malekpour</a>
 * @version 1, 12 Feb 2019
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Document(collection = "basic")
public class Basic extends Entity {
    @Field
    private String titleType;
    @Field
    private String primaryTitle;
    @Field
    private String originalTitle;
    @Field
    private Boolean isAdult;
    @Field
    private Integer startYear;
    @Field
    private Integer endYear;
    @Field
    private Integer runtimeMinutes;
    @Field
    private List<String> genres = new ArrayList<>();
}
