package com.amin.imdb.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import java.util.ArrayList;
import java.util.List;

/**
 * @author <a href="amin.malekpour@hotmail.com">Amin Malekpour</a>
 * @version 1, 12 Feb 2019
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Document(collection = "name")
public class Name extends Entity {
    @Field
    @Indexed
    private String primaryName;
    @Field
    private Integer birthYear;
    @Field
    private Integer deathYear;
    @Field
    private List<String> primaryProfession = new ArrayList<>();
    @Field
    private List<String> knownForTitles = new ArrayList<>();
}
