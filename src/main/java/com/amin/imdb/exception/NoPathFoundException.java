package com.amin.imdb.exception;

/**
 * @author <a href="amin.malekpour@hotmail.com">Amin Malekpour</a>
 * @version 1, 17 Feb 2019
 */
public class NoPathFoundException extends RuntimeException {

    public NoPathFoundException(String message) {
        super(message);
    }
}
