package com.amin.imdb.exception;

/**
 * @author <a href="amin.malekpour@hotmail.com">Amin Malekpour</a>
 * @version 1, 13 Feb 2019
 */
public class InvalidRequestException extends RuntimeException {

    public InvalidRequestException(String message) {
        super(message);
    }
}
