package com.amin.imdb.config;

/**
 * @author <a href="amin.malekpour@hotmail.com">Amin Malekpour</a>
 * @version 1, 13 Feb 2019
 */
public final class Constants {

    public static final int READING_STEP = 50000;
    public static final String KEVIN_BACON_ID = "nm0000102";

    public static final String VALUE_SEPARATOR = ",";
    public static final String COLUMN_SEPARATOR = "\\t";
    public static final String NULL = "\\N";
    public static final String ID_FORMAT = "%s_%s";

    public static final String FILE_AKA = "aka.tsv";
    public static final String FILE_BASIC = "basic.tsv";
    public static final String FILE_CREW = "crew.tsv";
    public static final String FILE_EPISODE = "episode.tsv";
    public static final String FILE_NAME = "name.tsv";
    public static final String FILE_PRINCIPAL = "principal.tsv";
    public static final String FILE_RATING = "rating.tsv";

    public static final String COLLECTION_AKA = "aka";
    public static final String COLLECTION_BASIC = "basic";
    public static final String COLLECTION_CREW = "crew";
    public static final String COLLECTION_EPISODE = "episode";
    public static final String COLLECTION_NAME = "name";
    public static final String COLLECTION_PRINCIPAL = "principal";
    public static final String COLLECTION_RATING = "rating";

    public static final String PERSON_DESCRIPTION = "Returns a list of person with detail for finding the personId.";
    public static final String TYPECAST_DESCRIPTION = "Inter personId, the system determined if the person has become typecast (at least half of their work is one genre).";
    public static final String COINCIDENCE_DESCRIPTION = "Inter 2 personIds, the system replies a list of movies or TV shows that both people have shared.";
    public static final String DEGREES_DESCRIPTION = "Inter personId, the system provides what’s the degree of separation between the given person and Kevin Bacon.";

    public static final String FILE_NOT_FOUND = "Can not find file %s";
}
