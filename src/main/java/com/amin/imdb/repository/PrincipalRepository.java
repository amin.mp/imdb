package com.amin.imdb.repository;

import com.amin.imdb.model.Principal;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

/**
 * @author <a href="amin.malekpour@hotmail.com">Amin Malekpour</a>
 * @version 1, 13 Feb 2019
 */
public interface PrincipalRepository extends MongoRepository<Principal, String> {

    List<Principal> findAllByNconst(String personId);

    Principal findByTconstAndNconst(String tconst, String nconst);

    List<Principal> findAllByTconst(String tconst);
}
