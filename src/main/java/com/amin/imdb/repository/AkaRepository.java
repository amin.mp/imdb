package com.amin.imdb.repository;

import com.amin.imdb.model.Aka;
import org.springframework.data.mongodb.repository.MongoRepository;

/**
 * @author <a href="amin.malekpour@hotmail.com">Amin Malekpour</a>
 * @version 1, 13 Feb 2019
 */
public interface AkaRepository extends MongoRepository<Aka, String> {
}
