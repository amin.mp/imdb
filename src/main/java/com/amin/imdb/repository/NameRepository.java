package com.amin.imdb.repository;

import com.amin.imdb.model.Name;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

/**
 * @author <a href="amin.malekpour@hotmail.com">Amin Malekpour</a>
 * @version 1, 13 Feb 2019
 */
public interface NameRepository extends MongoRepository<Name, String> {

    List<Name> findByPrimaryName(String name);
}
