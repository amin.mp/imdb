package com.amin.imdb.repository;

import com.amin.imdb.model.Basic;
import org.springframework.data.mongodb.repository.MongoRepository;

/**
 * @author <a href="amin.malekpour@hotmail.com">Amin Malekpour</a>
 * @version 1, 13 Feb 2019
 */
public interface BasicRepository extends MongoRepository<Basic, String> {
}
