package com.amin.imdb.controller;

import com.amin.imdb.config.Constants;
import com.amin.imdb.service.ImdbService;
import com.amin.imdb.service.dto.MovieDto;
import com.amin.imdb.service.dto.PersonDto;
import com.amin.imdb.service.dto.TypecastDto;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author <a href="amin.malekpour@hotmail.com">Amin Malekpour</a>
 * @version 1, 17 Feb 2019
 */
@RestController
@RequestMapping(value = "/api/imdb/")
@Api(description = "Provides set of REST APIs to cover the requests.")
public class ImdbController {

    @Autowired
    private ImdbService imdbService;

    @GetMapping("person/{name}")
    @ApiOperation(Constants.PERSON_DESCRIPTION)
    public List<PersonDto> findName(@PathVariable("name") String name) {
        return imdbService.findAllPerson(name);
    }

    @GetMapping("typecasting/{personId}")
    @ApiOperation(Constants.TYPECAST_DESCRIPTION)
    public TypecastDto findTypecast(@PathVariable("personId") String personId) {
        return imdbService.findTypecast(personId);
    }

    @GetMapping("coincidence")
    @ApiOperation(Constants.COINCIDENCE_DESCRIPTION)
    public List<MovieDto> findCoincidence(@RequestParam("personId1") String personId1,
                                          @RequestParam("personId2") String personId2) {
        return imdbService.findCoincidence(personId1, personId2);
    }

    @GetMapping("degrees/{personId}")
    @ApiOperation(Constants.DEGREES_DESCRIPTION)
    public List<String> findDegrees(@PathVariable("personId") String personId) {
        return imdbService.findDegrees(personId);
    }
}
