package com.amin.imdb.controller;

import com.amin.imdb.service.FillDatabaseService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

/**
 * @author <a href="amin.malekpour@hotmail.com">Amin Malekpour</a>
 * @version 1, 13 Feb 2019
 */
@RestController
@RequestMapping(value = "/api/fillDatabase")
@Api(description = "Provides set of REST APIs to handle Database.")
public class FillDatabaseController {

    @Autowired
    private FillDatabaseService fillDatabaseService;

    @PostMapping
    @ApiOperation("Use this for filling Database, run ONCE please." +
            "Insert new records into Database by reading from the files. " +
            "Before using this endpoint please download, extract and rename files then put " +
            "them in a directory and define the directory in application.properties")
    public ResponseEntity<?> fillDatabase() {
        fillDatabaseService.fill();
        return ResponseEntity.noContent().build();
    }
}
