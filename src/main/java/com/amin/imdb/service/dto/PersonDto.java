package com.amin.imdb.service.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * @author <a href="amin.malekpour@hotmail.com">Amin Malekpour</a>
 * @version 1, 18 Feb 2019
 */
@Data
@AllArgsConstructor
public class PersonDto implements Serializable {
    private String personId;
    private String name;
    private Integer birthYear;
    private Integer deathYear;
    private List<String> professions;
}
