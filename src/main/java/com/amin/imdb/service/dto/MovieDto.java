package com.amin.imdb.service.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.List;

/**
 * @author <a href="amin.malekpour@hotmail.com">Amin Malekpour</a>
 * @version 1, 17 Feb 2019
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class MovieDto implements Serializable {
    private String titleId;
    private String title;
    private String type;
    private List<String> genres;
}
