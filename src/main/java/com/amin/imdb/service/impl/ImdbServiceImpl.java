package com.amin.imdb.service.impl;

import com.amin.imdb.config.Constants;
import com.amin.imdb.exception.NoPathFoundException;
import com.amin.imdb.exception.NotFoundException;
import com.amin.imdb.model.Basic;
import com.amin.imdb.model.Name;
import com.amin.imdb.model.Principal;
import com.amin.imdb.repository.BasicRepository;
import com.amin.imdb.repository.NameRepository;
import com.amin.imdb.repository.PrincipalRepository;
import com.amin.imdb.service.ImdbService;
import com.amin.imdb.service.dto.MovieDto;
import com.amin.imdb.service.dto.PersonDto;
import com.amin.imdb.service.dto.TypecastDto;
import com.google.common.collect.Lists;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.*;
import java.util.stream.Collectors;

/**
 * @author <a href="amin.malekpour@hotmail.com">Amin Malekpour</a>
 * @version 1, 17 Feb 2019
 */
@Service
@Slf4j
public class ImdbServiceImpl implements ImdbService {

    @Autowired
    private NameRepository nameRepository;

    @Autowired
    private BasicRepository basicRepository;

    @Autowired
    private PrincipalRepository principalRepository;

    @Override
    public List<PersonDto> findAllPerson(String name) {
        List<Name> names = nameRepository.findByPrimaryName(name);
        if(names.isEmpty()) throw new NotFoundException(("Person not found"));
        List<PersonDto> result = names.stream().map(r ->
                new PersonDto(r.getId(), r.getPrimaryName(), r.getBirthYear(), r.getDeathYear(), r.getPrimaryProfession())
        ).collect(Collectors.toList());
        return result;
    }

    @Override
    public TypecastDto findTypecast(String personId) {
        Name name = nameRepository.findById(personId).orElseThrow(() -> new NotFoundException("personId not found"));
        return createTypecastDto(name, findBasics(personId));
    }

    @Override
    public List<MovieDto> findCoincidence(String personId1, String personId2) {
        List<Principal> principals1 = principalRepository.findAllByNconst(personId1);
        List<Principal> principals2 = principalRepository.findAllByNconst(personId2);
        if(principals1.isEmpty() || principals2.isEmpty()) return new ArrayList<>();
        List<String> titleIds = new ArrayList<>();
        for (Principal principal1 : principals1){
            for (Principal principal2 : principals2){
                if(principal1.getTconst().equalsIgnoreCase(principal2.getTconst())) {
                    titleIds.add(principal1.getTconst());
                    break;
                }
            }
        }
        List<Basic> basics = Lists.newArrayList(basicRepository.findAllById(titleIds));
        return mapToMovies(basics);
    }

    @Override
    public List<String> findDegrees(String personId) {
        Name person = nameRepository.findById(personId).orElseThrow(() -> new NotFoundException(("personId not found")));
        Name kevinBacon = nameRepository.findById(Constants.KEVIN_BACON_ID).orElseThrow(() -> new IllegalStateException(("Data is not filled")));
        Map<String, String> parent = new HashMap<>();

        Queue<NodeAndDepth> nameQueue = new ArrayDeque<>();
        nameQueue.add(new NodeAndDepth(person.getId(), 0));
        parent.put(person.getId(), "");
        int degree = calculateDistance(nameQueue, kevinBacon.getId(), parent);
        List<String> path = new ArrayList<>();
        if (parent.containsKey(kevinBacon.getId())) {
            log.info("Degree of separation of Kevin Bacon is {} for {}", degree, person.getPrimaryName());
            String node = kevinBacon.getId();
            while (!StringUtils.isEmpty(node)) {
                path.add(nameRepository.findById(node).map(Name::getPrimaryName).orElse(null));
                if(!node.equals(parent.get(node))){
                    node = parent.get(node);
                }else{
                    node = null;
                }
            }
        }else{
            log.info("Degree of separation of Kevin Bacon is not found for {}", person.getPrimaryName());
        }
        Collections.reverse(path);
        return path;
    }

    private TypecastDto createTypecastDto(Name name, List<Basic> basics) {
        TypecastDto result = new TypecastDto(name.getId(), name.getPrimaryName(), false, mapToMovies(basics));
        int size = result.getMovies().size();
        if(size == 0) return result;
        if(size == 1 || size == 2) {
            result.setTypecast(true);
            return result;
        }

        for (int i = 0; i <= (size / 2); i++) {
            for(String genre1 : result.getMovies().get(i).getGenres()){
                int found = 1;
                for (int j = i + 1; j < size; j++) {
                    for(String genre2 : result.getMovies().get(j).getGenres()){
                        if (genre1.equalsIgnoreCase(genre2)) {
                            found++;
                            if (0.5 <= Float.valueOf(found) / size) {
                                result.setTypecast(true);
                                return result;
                            }
                            break;
                        }
                    }
                }
            }
        }
        return result;
    }

    /**
     * Find Movies with actor/actress personId.
     * @param personId
     * @return list of basic
     */
    private List<Basic> findBasics(String personId) {
        List<Principal> principals = principalRepository.findAllByNconst(personId);
        if (principals.isEmpty()) return new ArrayList<>();
        List<String> titleIds = principals.stream().map(principal -> principal.getTconst()).collect(Collectors.toList());
        if (titleIds.isEmpty()) return new ArrayList<>();
        List<Basic> basics = Lists.newArrayList(basicRepository.findAllById(titleIds));
        return basics;
    }

    /**
     * For mapping list of Basics to list of Movies
     * @param basics
     * @return list of Movies
     */
    private List<MovieDto> mapToMovies(List<Basic> basics){
        List<MovieDto> result = new ArrayList<>();
        for (Basic basic : basics) {
            MovieDto movie = new MovieDto(basic.getId(), basic.getPrimaryTitle(), basic.getTitleType(), basic.getGenres());
            result.add(movie);
        }
        return result;
    }

    private int calculateDistance(Queue<NodeAndDepth> nameQueue, String target, Map<String, String> parent) {
        int depth = 0;
        while (!nameQueue.isEmpty()) {
            NodeAndDepth currentNode = nameQueue.poll();
            if (currentNode.getDepth() < 6) {
                for (Principal principal : principalRepository.findAllByNconst(currentNode.getName())) {
                    if (principalRepository.findByTconstAndNconst(principal.getTconst(), target) != null) {
                        parent.put(target, currentNode.getName());
                        return depth;
                    } else {
                        nameQueue.addAll((principalRepository.findAllByTconst(principal.getTconst())
                                .stream()
                                .map(Principal::getNconst)
                                .map(name -> new NodeAndDepth(name, currentNode.getDepth() + 1))
                                .filter(node -> !parent.containsKey(node.getName()))
                                .peek(node -> parent.put(node.getName(), currentNode.getName()))
                                .collect(Collectors.toList())));
                    }
                }
            }
        }
        throw new NoPathFoundException("Degree is more than 6.");
    }

    @AllArgsConstructor
    @Data
    class NodeAndDepth {
        String name;
        int depth;
    }

}
