package com.amin.imdb.service.impl;

import com.amin.imdb.config.Constants;
import com.amin.imdb.model.*;
import com.amin.imdb.service.FillDatabaseService;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.ConcurrentModificationException;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * @author <a href="amin.malekpour@hotmail.com">Amin Malekpour</a>
 * @version 1, 13 Feb 2019
 */
@Service
@Slf4j
public class FillDatabaseServiceImpl implements FillDatabaseService {

    @Autowired
    private MongoTemplate mongoTemplate;

    @Value("${file.directory}")
    private String fileDir;

    private ExecutorService executor = Executors.newFixedThreadPool(7);

    List<Future> futures = new CopyOnWriteArrayList<>();

    @Override
    public void fill() {
        validateAllFiles();
        if (futures.stream().anyMatch(f -> !f.isDone()))
            throw new ConcurrentModificationException("Another job is filling Database.");

        futures.add(executor.submit(() -> insertToMongo(Constants.COLLECTION_NAME, fileDir + Constants.FILE_NAME, this::mapName)));
        futures.add(executor.submit(() -> insertToMongo(Constants.COLLECTION_PRINCIPAL, fileDir + Constants.FILE_PRINCIPAL, this::mapPrincipal)));
        futures.add(executor.submit(() -> insertToMongo(Constants.COLLECTION_BASIC, fileDir + Constants.FILE_BASIC, this::mapBasic)));

        //For current business these are not necessary
        futures.add(executor.submit(() -> insertToMongo(Constants.COLLECTION_AKA, fileDir + Constants.FILE_AKA, this::mapAka)));
        futures.add(executor.submit(() -> insertToMongo(Constants.COLLECTION_CREW, fileDir + Constants.FILE_CREW, this::mapCrew)));
        futures.add(executor.submit(() -> insertToMongo(Constants.COLLECTION_EPISODE, fileDir + Constants.FILE_EPISODE, this::mapEpisode)));
        futures.add(executor.submit(() -> insertToMongo(Constants.COLLECTION_RATING, fileDir + Constants.FILE_RATING, this::mapRating)));
    }

    /**
     * For reading from file and map to records and finally insert into a specified mongo collection.
     *
     * @param collection
     * @param filePath
     * @param mapperFunction
     */
    @SneakyThrows
    private void insertToMongo(String collection, String filePath, Function<String, Object> mapperFunction) {
        log.info("Reading file from '.{}' started.", filePath);
        File file = new File(filePath);
        try (InputStream inputStream = new FileInputStream(file);
             BufferedReader br = new BufferedReader(new InputStreamReader(inputStream, StandardCharsets.UTF_8))) {
            int totalBytes = inputStream.available();
            float progress;
            List<String> lines = readLines(br, Constants.READING_STEP);
            if (!CollectionUtils.isEmpty(lines)) lines.remove(0);
            log.info("Inserting records to '{}' collection started.", collection);
            int step = 0;
            int total = 0;
            while (!CollectionUtils.isEmpty(lines)) {
                List list = lines.stream().map(mapperFunction).collect(Collectors.toList());
                mongoTemplate.insert(list, collection);
                progress = ((totalBytes - inputStream.available()) / Float.valueOf(totalBytes)) * 100;
                log.info("Step {}: {}% completed, {} records inserted into '{}' collection. Total inserted records = {}",
                        ++step, String.format("%.2f", progress), lines.size(), collection, total += list.size());
                lines = readLines(br, Constants.READING_STEP);
            }
            log.info("Inserting records to '{}' collection finished. Total inserted records = {}", collection, total);
        }
        log.info("Reading file from '.{}' finished.", file);
    }

    /**
     * For mapping a line of Sting to Aka Object
     *
     * @param line
     * @return Aka
     */
    public Aka mapAka(String line) {
        String[] columns = line.split(Constants.COLUMN_SEPARATOR);
        Aka record = new Aka();
        if (!Constants.NULL.equals(columns[0])) record.setTitleId(columns[0]);
        else return null;
        if (!Constants.NULL.equals(columns[1])) record.setOrdering(Integer.valueOf(columns[1]));
        else return null;
        record.setId(String.format(Constants.ID_FORMAT, record.getTitleId(), record.getOrdering()));
        if (!Constants.NULL.equals(columns[1])) record.setOrdering(Integer.valueOf(columns[1]));
        if (!Constants.NULL.equals(columns[2])) record.setTitle(columns[2]);
        if (!Constants.NULL.equals(columns[3])) record.setRegion(columns[3]);
        if (!Constants.NULL.equals(columns[4])) record.setLanguage(columns[4]);

        if (!Constants.NULL.equals(columns[5]) && !columns[5].isEmpty())
            record.setType(Arrays.asList(columns[5].split(Constants.VALUE_SEPARATOR)));
        else
            record.setType(new ArrayList<>());

        if (!Constants.NULL.equals(columns[6]) && !columns[6].isEmpty())
            record.setAttribute(Arrays.asList(columns[6].split(Constants.VALUE_SEPARATOR)));
        else
            record.setAttribute(new ArrayList<>());

        if (!Constants.NULL.equals(columns[7])) record.setIsOriginalTitle(Boolean.valueOf(columns[7]));

        return record;
    }

    /**
     * For mapping a line of Sting to Basic Object
     *
     * @param line
     * @return Basic
     */
    public Basic mapBasic(String line) {
        String[] columns = line.split(Constants.COLUMN_SEPARATOR);
        Basic record = new Basic();
        if (!Constants.NULL.equals(columns[0])) record.setId(columns[0]);
        else return null;
        if (!Constants.NULL.equals(columns[1])) record.setTitleType(columns[1]);
        if (!Constants.NULL.equals(columns[2])) record.setPrimaryTitle(columns[2]);
        if (!Constants.NULL.equals(columns[3])) record.setOriginalTitle(columns[3]);
        if (!Constants.NULL.equals(columns[4])) record.setIsAdult(Boolean.valueOf(columns[4]));
        if (!Constants.NULL.equals(columns[5])) record.setStartYear(Integer.valueOf(columns[5]));
        if (!Constants.NULL.equals(columns[6])) record.setEndYear(Integer.valueOf(columns[6]));
        if (!Constants.NULL.equals(columns[7])) record.setRuntimeMinutes(Integer.valueOf(columns[7]));
        if (!Constants.NULL.equals(columns[8]) && !columns[8].isEmpty())
            record.setGenres(Arrays.asList(columns[8].split(Constants.VALUE_SEPARATOR)));
        else
            record.setGenres(new ArrayList<>());

        return record;
    }

    /**
     * For mapping a line of Sting to Crew Object
     *
     * @param line
     * @return Crew
     */
    public Crew mapCrew(String line) {
        String[] columns = line.split(Constants.COLUMN_SEPARATOR);
        Crew record = new Crew();
        if (!Constants.NULL.equals(columns[0])) record.setId(columns[0]);
        else return null;

        if (!Constants.NULL.equals(columns[1]) && !columns[1].isEmpty())
            record.setDirectors(Arrays.asList(columns[1].split(Constants.VALUE_SEPARATOR)));
        else
            record.setDirectors(new ArrayList<>());

        if (!Constants.NULL.equals(columns[2]) && !columns[2].isEmpty())
            record.setWriters(Arrays.asList(columns[2].split(Constants.VALUE_SEPARATOR)));
        else
            record.setWriters(new ArrayList<>());

        return record;
    }

    /**
     * For mapping a line of Sting to Episode Object
     *
     * @param line
     * @return Episode
     */
    public Episode mapEpisode(String line) {
        String[] columns = line.split(Constants.COLUMN_SEPARATOR);
        Episode record = new Episode();
        if (!Constants.NULL.equals(columns[0])) record.setId(columns[0]);
        else return null;
        if (!Constants.NULL.equals(columns[1])) record.setParentTconst(columns[1]);
        if (!Constants.NULL.equals(columns[2])) record.setSeasonNumber(Integer.valueOf(columns[2]));
        if (!Constants.NULL.equals(columns[3])) record.setEpisodeNumber(Integer.valueOf(columns[3]));
        return record;
    }

    /**
     * For mapping a line of Sting to Name Object
     *
     * @param line
     * @return Name
     */
    public Name mapName(String line) {
        String[] columns = line.split(Constants.COLUMN_SEPARATOR);
        Name record = new Name();
        if (!Constants.NULL.equals(columns[0])) record.setId(columns[0]);
        else return null;
        if (!Constants.NULL.equals(columns[1])) record.setPrimaryName(columns[1]);
        if (!Constants.NULL.equals(columns[2])) record.setBirthYear(Integer.valueOf(columns[2]));
        if (!Constants.NULL.equals(columns[3])) record.setDeathYear(Integer.valueOf(columns[3]));

        if (!Constants.NULL.equals(columns[4]) && !columns[4].isEmpty())
            record.setPrimaryProfession(Arrays.asList(columns[4].split(Constants.VALUE_SEPARATOR)));
        else
            record.setPrimaryProfession(new ArrayList<>());

        if (!Constants.NULL.equals(columns[5]) && !columns[5].isEmpty())
            record.setKnownForTitles(Arrays.asList(columns[5].split(Constants.VALUE_SEPARATOR)));
        else
            record.setKnownForTitles(new ArrayList<>());

        return record;
    }

    /**
     * For mapping a line of Sting to Principal Object
     *
     * @param line
     * @return Principal
     */
    public Principal mapPrincipal(String line) {
        String[] columns = line.split(Constants.COLUMN_SEPARATOR);
        Principal record = new Principal();
        if (!Constants.NULL.equals(columns[0])) record.setTconst(columns[0]);
        else return null;
        if (!Constants.NULL.equals(columns[1])) record.setOrdering(Integer.valueOf(columns[1]));
        else return null;
        record.setId(String.format(Constants.ID_FORMAT, record.getTconst(), record.getOrdering()));
        if (!Constants.NULL.equals(columns[2])) record.setNconst(columns[2]);
        if (!Constants.NULL.equals(columns[3])) record.setCategory(columns[3]);
        if (!Constants.NULL.equals(columns[4])) record.setJob(columns[4]);
        if (!Constants.NULL.equals(columns[5])) record.setCharacters(columns[5]);
        return record;
    }

    /**
     * For mapping a line of Rating to Rating Object
     *
     * @param line
     * @return Rating
     */
    public Rating mapRating(String line) {
        String[] columns = line.split(Constants.COLUMN_SEPARATOR);
        Rating record = new Rating();
        if (!Constants.NULL.equals(columns[0])) record.setId(columns[0]);
        else return null;
        if (!Constants.NULL.equals(columns[1])) record.setAverageRating(Float.valueOf(columns[1]));
        if (!Constants.NULL.equals(columns[2])) record.setNumVotes(Long.valueOf(columns[2]));
        return record;
    }

    /**
     * For reading lines.
     *
     * @param reader
     * @param numberOfLines
     * @return list of lines
     */
    @SneakyThrows
    private List<String> readLines(BufferedReader reader, int numberOfLines) {
        List<String> result = new ArrayList<>();
        String line;
        while (numberOfLines > 0 && (line = reader.readLine()) != null) {
            result.add(line);
            numberOfLines--;
        }
        return result;
    }

    @SneakyThrows
    private void validateAllFiles() {
        String name = String.format("%s%s", fileDir, Constants.FILE_NAME);
        File file = new File(name);
        if (!file.exists()) throw new FileNotFoundException(String.format(Constants.FILE_NOT_FOUND, name));

        String principal = String.format("%s%s", fileDir, Constants.FILE_PRINCIPAL);
        file = new File(principal);
        if (!file.exists()) throw new FileNotFoundException(String.format(Constants.FILE_NOT_FOUND, principal));

        String basic = String.format("%s%s", fileDir, Constants.FILE_BASIC);
        file = new File(basic);
        if (!file.exists()) throw new FileNotFoundException(String.format(Constants.FILE_NOT_FOUND, basic));

        //For current business these are not necessary
        String aka = String.format("%s%s", fileDir, Constants.FILE_AKA);
        file = new File(aka);
        if (!file.exists()) throw new FileNotFoundException(String.format(Constants.FILE_NOT_FOUND, aka));

        String crew = String.format("%s%s", fileDir, Constants.FILE_CREW);
        file = new File(crew);
        if (!file.exists()) throw new FileNotFoundException(String.format(Constants.FILE_NOT_FOUND, crew));

        String episode = String.format("%s%s", fileDir, Constants.FILE_EPISODE);
        file = new File(episode);
        if (!file.exists()) throw new FileNotFoundException(String.format(Constants.FILE_NOT_FOUND, episode));

        String rating = String.format("%s%s", fileDir, Constants.FILE_RATING);
        file = new File(rating);
        if (!file.exists()) throw new FileNotFoundException(String.format(Constants.FILE_NOT_FOUND, rating));
    }

}
