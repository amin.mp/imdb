package com.amin.imdb.service;

import com.amin.imdb.service.dto.MovieDto;
import com.amin.imdb.service.dto.PersonDto;
import com.amin.imdb.service.dto.TypecastDto;

import java.util.List;

/**
 * @author <a href="amin.malekpour@hotmail.com">Amin Malekpour</a>
 * @version 1, 17 Feb 2019
 */
public interface ImdbService {

    /**
     * For finding persons by a given name.
     * @param name
     * @return list of PersonDto
     */
    List<PersonDto> findAllPerson(String name);


    /**
     * For finding if person has become typecasted.
     * @param id
     * @return list of TypecastDto
     */
    TypecastDto findTypecast(String id);

    /**
     * For finding movies or TV shows that both people have shared.
     * @param id1
     * @param id2
     * @return list of MovieDto
     */
    List<MovieDto> findCoincidence(String id1, String id2);

    /**
     * For finding Six degrees of Kevin Bacon
     * @param id
     * @return list of names
     */
    List<String> findDegrees(String id);
}
