package com.amin.imdb.service;

/**
 * @author <a href="amin.malekpour@hotmail.com">Amin Malekpour</a>
 * @version 1, 13 Feb 2019
 */
public interface FillDatabaseService {

    /**
     * For filling database by reading from the files.
     */
    void fill();
}
