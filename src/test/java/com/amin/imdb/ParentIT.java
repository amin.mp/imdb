package com.amin.imdb;

import com.amin.imdb.config.Constants;
import com.amin.imdb.model.Basic;
import com.amin.imdb.model.Name;
import com.amin.imdb.model.Principal;
import com.amin.imdb.repository.*;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.mongodb.MongoDbFactory;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.gridfs.GridFsTemplate;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Optional;

import static org.mockito.BDDMockito.given;

/**
 * @author <a href="amin.malekpour@hotmail.com">Amin Malekpour</a>
 * @version 1, 18 Feb 2019
 */
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT,
        properties="spring.autoconfigure.exclude=org.springframework.boot.autoconfigure.mongo.MongoAutoConfiguration")

public class ParentIT {
    @MockBean
    protected NameRepository nameRepository;

    @MockBean
    protected PrincipalRepository principalRepository;

    @MockBean
    protected AkaRepository akaRepository;

    @MockBean
    protected CrewRepository crewRepository;

    @MockBean
    protected BasicRepository basicRepository;

    @MockBean
    protected EpisodeRepository episodeRepository;

    @MockBean
    protected RatingRepository ratingRepository;

    @MockBean
    protected MongoDbFactory mongoDbFactory;

    @MockBean
    protected MongoTemplate mongoTemplate;

    @MockBean
    protected GridFsTemplate gridFsTemplate;

    protected void initialize(){
        //name_1
        Name name_1 = new Name("Gregory Deutsch", null,  null,
                new ArrayList<>(), Arrays.asList("tt0168013,tt0051303,tt0048907".split(",")));
        name_1.setId("nm10237589");

        given(nameRepository.findByPrimaryName("Gregory Deutsch")).willReturn(Arrays.asList(name_1));
        given(nameRepository.findById("nm10237589")).willReturn(Optional.of(name_1));

        //principal_1 for name_1
        Principal principal_1 = new Principal("tt0168013", 10, "nm10237589", "self", null,
                "[\"Himself\"]");
        principal_1.setId(String.format(Constants.ID_FORMAT, "tt0168013", 10));

        given(principalRepository.findByTconstAndNconst("tt0168013", "nm10237589")).willReturn(principal_1);

        //principal_2 for name_1
        Principal principal_2 = new Principal("tt9233738", 2, "nm10237589", "self", null,
                "[\"Himself\"]");
        principal_2.setId(String.format(Constants.ID_FORMAT, "tt9233738", 2));

        given(principalRepository.findByTconstAndNconst("tt9233738", "nm10237589")).willReturn(principal_2);

        //principal_3 for name_1
        Principal principal_3 = new Principal("tt9251660", 2, "nm10237589", "self", null,
                "[\"Himself\"]");
        principal_3.setId(String.format(Constants.ID_FORMAT, "tt9251660", 2));

        given(principalRepository.findByTconstAndNconst("tt9251660", "nm10237589")).willReturn(principal_3);
        given(principalRepository.findAllByNconst("nm10237589")).willReturn(Arrays.asList(principal_1, principal_2, principal_3));

        //name_2
        Name name_2 = new Name("Robert Q. Lewis", 1920,  1991,
                Arrays.asList("actor,soundtrack".split(",")),
                Arrays.asList("tt0050105,tt0061791,tt0068555,tt0078924".split(",")));
        name_2.setId("nm0507691");

        given(nameRepository.findByPrimaryName("Robert Q. Lewis")).willReturn(Arrays.asList(name_2));
        given(nameRepository.findById("nm0507691")).willReturn(Optional.of(name_2));

        //principal_4 for name_2
        Principal principal_4 = new Principal("tt9233738", 3, "nm0507691", "self", null,
                "[\"Himself - Host\"]");
        principal_4.setId(String.format(Constants.ID_FORMAT, "tt9233738", 3));

        given(principalRepository.findAllByNconst("nm0507691")).willReturn(Arrays.asList(principal_4));
        given(principalRepository.findByTconstAndNconst("tt9233738", "nm0507691")).willReturn(principal_4);

        //principal_5 for another person
        Principal principal_5 = new Principal("tt9233738", 1, "nm0647692", "self", null,
                " [\"Himself - Announcer\"]");
        principal_5.setId(String.format(Constants.ID_FORMAT, "tt9233738", 1));

        given(principalRepository.findAllByNconst("nm0647692")).willReturn(Arrays.asList(principal_5));
        given(principalRepository.findByTconstAndNconst("tt9233738", "nm0647692")).willReturn(principal_5);

        //principal by same id = tt9233738
        given(principalRepository.findAllByTconst("tt9233738")).willReturn(Arrays.asList(principal_2, principal_4, principal_5));

        //basic_1 for name_1
        Basic basic_1 = new Basic("tvSeries", "The Joe Franklin Show", "The Joe Franklin Show", false, 1950, 1993,
                60, Arrays.asList("Talk-Show"));
        basic_1.setId("tt0168013");

        //basic_2 for name_1
        Basic basic_2 = new Basic("tvEpisode", "Episode dated 10 January 1963",
                "Episode dated 10 January 1963", false, 1963, null,
                null, new ArrayList<>());
        basic_2.setId("tt9233738");

        //basic_3 for name_1
        Basic basic_3 = new Basic("tvEpisode", "Joe Franklin's Memory Lane",
                "Joe Franklin's Memory Lane", false, 1965, null,
                null, Arrays.asList("Talk-Show"));
        basic_3.setId("tt9251660");

        given(basicRepository.findById("tt0168013")).willReturn(Optional.of(basic_1));
        given(basicRepository.findById("tt9233738")).willReturn(Optional.of(basic_2));
        given(basicRepository.findById("tt9251660")).willReturn(Optional.of(basic_3));

        given(basicRepository.findAllById(Arrays.asList("tt0168013"))).willReturn(Arrays.asList(basic_1));
        given(basicRepository.findAllById(Arrays.asList("tt9233738"))).willReturn(Arrays.asList(basic_2));
        given(basicRepository.findAllById(Arrays.asList("tt9251660"))).willReturn(Arrays.asList(basic_3));

        given(basicRepository.findAllById(Arrays.asList("tt0168013", "tt9233738"))).willReturn(Arrays.asList(basic_1, basic_2));
        given(basicRepository.findAllById(Arrays.asList("tt0168013", "tt9251660"))).willReturn(Arrays.asList(basic_1, basic_3));
        given(basicRepository.findAllById(Arrays.asList("tt9233738", "tt9251660"))).willReturn(Arrays.asList(basic_2, basic_3));

        given(basicRepository.findAllById(Arrays.asList("tt0168013", "tt9233738", "tt9251660"))).willReturn(Arrays.asList(basic_1, basic_2, basic_3));
    }

    protected void initializeForDegree(){
        //name_1
        Name name_1 = new Name("Kevin Bacon", 1958,  null,
                Arrays.asList("actor,producer,soundtrack".split(",")),
                Arrays.asList("tt0327056,tt0361127,tt0164052,tt0087277".split(",")));
        name_1.setId("nm0000102");

        given(nameRepository.findByPrimaryName("Kevin Baconh")).willReturn(Arrays.asList(name_1));
        given(nameRepository.findById("nm0000102")).willReturn(Optional.of(name_1));

        //name_2
        Name name_2 = new Name("Glenn Ford", 1916,  2006,
                Arrays.asList("actor,producer,soundtrack".split(",")),
                Arrays.asList("tt0038559,tt0078346,tt0055312,tt0056956".split(",")));
        name_2.setId("nm0001229");

        given(nameRepository.findByPrimaryName("Glenn Ford")).willReturn(Arrays.asList(name_2));
        given(nameRepository.findById("nm0001229")).willReturn(Optional.of(name_2));

        //name_3
        Name name_3 = new Name("Dalton Trumbo", 1905,  1976,
                Arrays.asList("writer,actor,director".split(",")),
                Arrays.asList("tt0070511,tt0054331,tt0046250,tt0067277".split(",")));
        name_3.setId("nm0874308");

        given(nameRepository.findByPrimaryName("alton Trumbo")).willReturn(Arrays.asList(name_3));
        given(nameRepository.findById("nm0874308")).willReturn(Optional.of(name_3));


        //principal_1 for name_1
        Principal principal_1 = new Principal("tt0079209", 1, "nm0000102", "actor", null,
                "[\"Teddy\"]");
        principal_1.setId(String.format(Constants.ID_FORMAT, "tt0079209", 1));

        given(principalRepository.findByTconstAndNconst("tt0079209", "nm0000102")).willReturn(principal_1);

        //principal_2 for name_1
        Principal principal_2 = new Principal("tt0083833", 3, "nm0000102", "actor", null,
                "[\"Timothy Fenwick Jr.\"]");
        principal_2.setId(String.format(Constants.ID_FORMAT, "tt0083833", 3));

        given(principalRepository.findByTconstAndNconst("tt0083833", "nm0000102")).willReturn(principal_2);
        given(principalRepository.findAllByNconst("nm0000102")).willReturn(Arrays.asList(principal_1, principal_2));

        //principal_3 for name_2
        Principal principal_3 = new Principal("tt0079209", 2, "nm0001229", "actor", null,
                "[\"Billy Devlin\"]");
        principal_3.setId(String.format(Constants.ID_FORMAT, "tt0079209", 2));

        given(principalRepository.findByTconstAndNconst("tt0079209", "nm0001229")).willReturn(principal_3);

        //principal_4 for name_2
        Principal principal_4 = new Principal("tt0031409", 4, "nm0001229", "actor", null,
                "[\"Joe Riley\"]");
        principal_4.setId(String.format(Constants.ID_FORMAT, "tt0031409", 4));

        given(principalRepository.findByTconstAndNconst("tt0031409", "nm0001229")).willReturn(principal_4);
        given(principalRepository.findAllByNconst("nm0001229")).willReturn(Arrays.asList(principal_3, principal_4));


        //principal_5 for name_3
        Principal principal_5 = new Principal("tt0031409", 6, "nm0874308", "writer",
                "screen play", null);
        principal_5.setId(String.format(Constants.ID_FORMAT, "tt0031409", 6));

        given(principalRepository.findByTconstAndNconst("tt0031409", "nm0874308")).willReturn(principal_5);
        given(principalRepository.findAllByNconst("nm0874308")).willReturn(Arrays.asList(principal_5));

        //principal by same id = tt0031409
        given(principalRepository.findAllByTconst("tt0031409")).willReturn(Arrays.asList(principal_4, principal_5));

        //principal by same id = tt0031409
        given(principalRepository.findAllByTconst("tt0079209")).willReturn(Arrays.asList(principal_1, principal_2));

        //name_4
        Name name_4 = new Name("valid name", null,  null,
                new ArrayList<>(),
                new ArrayList<>());

        name_4.setId("validId");

        given(nameRepository.findByPrimaryName("valid name")).willReturn(Arrays.asList(name_4));
        given(nameRepository.findById("validId")).willReturn(Optional.of(name_4));
    }
}
