package com.amin.imdb.service.impl;

import com.amin.imdb.ParentIT;
import com.amin.imdb.exception.NoPathFoundException;
import com.amin.imdb.exception.NotFoundException;
import com.amin.imdb.service.ImdbService;
import com.amin.imdb.service.dto.MovieDto;
import com.amin.imdb.service.dto.PersonDto;
import com.amin.imdb.service.dto.TypecastDto;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.Matchers.hasSize;
import static org.junit.Assert.assertThat;

/**
 * @author <a href="amin.malekpour@hotmail.com">Amin Malekpour</a>
 * @version 1, 18 Feb 2019
 */
@RunWith(SpringRunner.class)
public class ImdbServiceImplTest extends ParentIT {

    @Autowired
    private ImdbService imdbService;

    @Before
    public void setUp(){
        initialize();
        initializeForDegree();
    }

    @Test
    public void findAllPerson_whenValidName_thenReturnListOfPersonDto() {
        List<PersonDto> result = imdbService.findAllPerson("Gregory Deutsch");

        assertThat(result, notNullValue());
        assertThat(result, hasSize(1));
        assertThat(result.get(0).getPersonId(), equalTo("nm10237589"));
        assertThat(result.get(0).getName(), equalTo("Gregory Deutsch"));
        assertThat(result.get(0).getBirthYear(), equalTo(null));
        assertThat(result.get(0).getDeathYear(), equalTo(null));
        assertThat(result.get(0).getProfessions(), notNullValue());
        assertThat(result.get(0).getProfessions(), hasSize(0));
    }

    @Test(expected = NotFoundException.class)
    public void findAllPerson_whenInvalidName_thenReturnNotFound() {
        imdbService.findAllPerson("invalidName");
    }

    @Test
    public void findTypecast_whenValidId_thenTypecastDtoShouldBeFound() {
        TypecastDto result = imdbService.findTypecast("nm10237589");

        assertThat(result, notNullValue());
        assertThat(result.getNameId(), equalTo("nm10237589"));
        assertThat(result.getName(), equalTo("Gregory Deutsch"));
        assertThat(result.isTypecast(), equalTo(true));
        assertThat(result.getMovies(), notNullValue());
        assertThat(result.getMovies(), hasSize(3));

        assertThat(result.getMovies().get(0).getTitleId(), equalTo("tt0168013"));
        assertThat(result.getMovies().get(0).getTitle(), equalTo("The Joe Franklin Show"));
        assertThat(result.getMovies().get(0).getType(), equalTo("tvSeries"));
        assertThat(result.getMovies().get(0).getGenres(), hasSize(1));
        assertThat(result.getMovies().get(0).getGenres().get(0), equalTo("Talk-Show"));

        assertThat(result.getMovies().get(1).getTitleId(), equalTo("tt9233738"));
        assertThat(result.getMovies().get(1).getTitle(), equalTo("Episode dated 10 January 1963"));
        assertThat(result.getMovies().get(1).getType(), equalTo("tvEpisode"));
        assertThat(result.getMovies().get(1).getGenres(), hasSize(0));

        assertThat(result.getMovies().get(2).getTitleId(), equalTo("tt9251660"));
        assertThat(result.getMovies().get(2).getTitle(), equalTo("Joe Franklin's Memory Lane"));
        assertThat(result.getMovies().get(2).getType(), equalTo("tvEpisode"));
        assertThat(result.getMovies().get(2).getGenres(), hasSize(1));
        assertThat(result.getMovies().get(2).getGenres().get(0), equalTo("Talk-Show"));
    }

    @Test(expected = NotFoundException.class)
    public void findTypecast_whenInvalidId_thenTypecastDtoShouldBeFound() {
        imdbService.findTypecast("invalidId");
    }

    @Test
    public void findCoincidence_whenValidIds_thenReturnListOfMovieDto() {
        List<MovieDto> result = imdbService.findCoincidence("nm10237589", "nm0507691");

        assertThat(result, notNullValue());
        assertThat(result, hasSize(1));
        assertThat(result.get(0).getTitleId(), equalTo("tt9233738"));
        assertThat(result.get(0).getTitle(), equalTo("Episode dated 10 January 1963"));
        assertThat(result.get(0).getType(), equalTo("tvEpisode"));
        assertThat(result.get(0).getGenres(), notNullValue());
        assertThat(result.get(0).getGenres(), hasSize(0));
    }

    @Test
    public void findCoincidence_whenValidIdsOrderChanged_thenReturnListOfMovieDto() {
        List<MovieDto> result = imdbService.findCoincidence("nm0507691", "nm10237589");

        assertThat(result, notNullValue());
        assertThat(result, hasSize(1));
        assertThat(result.get(0).getTitleId(), equalTo("tt9233738"));
        assertThat(result.get(0).getTitle(), equalTo("Episode dated 10 January 1963"));
        assertThat(result.get(0).getType(), equalTo("tvEpisode"));
        assertThat(result.get(0).getGenres(), notNullValue());
        assertThat(result.get(0).getGenres(), hasSize(0));
    }

    @Test
    public void findCoincidence_whenInvalidIds_thenReturnEmptyList() {
        List<MovieDto> result = imdbService.findCoincidence("invalidId2", "invalidId2");
        assertThat(result, notNullValue());
        assertThat(result, hasSize(0));
    }

    @Test
    public void findCoincidence_whenFirstIdIsInvalid_thenReturnEmptyList() {
        List<MovieDto> result = imdbService.findCoincidence("invalidId_1", "tt9233738");
        assertThat(result, notNullValue());
        assertThat(result, hasSize(0));
    }

    @Test
    public void findCoincidence_whenSecondIdIsInvalid_thenReturnEmptyList() {
        List<MovieDto> result = imdbService.findCoincidence("tt9233738", "invalidId_2");
        assertThat(result, notNullValue());
        assertThat(result, hasSize(0));
    }

    @Test
    public void findDegrees_whenValidId_thenReturnListSizeTwo() {
        List<String> result = imdbService.findDegrees("nm0001229");

        assertThat(result, notNullValue());
        assertThat(result, hasSize(2));
        assertThat(result.get(0), equalTo("Glenn Ford"));
        assertThat(result.get(1), equalTo("Kevin Bacon"));
    }

    @Test
    public void findDegrees_whenValidId_thenReturnListSizeThree() {
        List<String> result = imdbService.findDegrees("nm0874308");

        assertThat(result, notNullValue());
        assertThat(result, hasSize(3));
        assertThat(result.get(0), equalTo("Dalton Trumbo"));
        assertThat(result.get(1), equalTo("Glenn Ford"));
        assertThat(result.get(2), equalTo("Kevin Bacon"));
    }

    @Test(expected = NotFoundException.class)
    public void findDegrees_whenInvalidId_thenNotFound() {
        imdbService.findDegrees("invalidId");
    }

    @Test(expected = NoPathFoundException.class)
    public void findDegrees_whenValidId_thenNoPathFound() {
        imdbService.findDegrees("validId");
    }

}
