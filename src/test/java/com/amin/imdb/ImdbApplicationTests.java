package com.amin.imdb;

import com.amin.imdb.controller.FillDatabaseController;
import com.amin.imdb.controller.ImdbController;
import com.amin.imdb.service.FillDatabaseService;
import com.amin.imdb.service.ImdbService;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
public class ImdbApplicationTests extends ParentIT {

	@Autowired
	private ApplicationContext applicationContext;

	@Test
	public void contextLoads() {
		Assert.assertNotNull(applicationContext.getBean(ImdbController.class));
		Assert.assertNotNull(applicationContext.getBean(FillDatabaseController.class));

		Assert.assertNotNull(applicationContext.getBean(ImdbService.class));
		Assert.assertNotNull(applicationContext.getBean(FillDatabaseService.class));
	}

}

