package com.amin.imdb.controller;

import com.amin.imdb.ParentIT;
import com.amin.imdb.service.dto.MovieDto;
import com.amin.imdb.service.dto.PersonDto;
import com.amin.imdb.service.dto.TypecastDto;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.Matchers.hasSize;
import static org.junit.Assert.assertThat;

/**
 * @author <a href="amin.malekpour@hotmail.com">Amin Malekpour</a>
 * @version 1, 18 Feb 2019
 */
@RunWith(SpringRunner.class)
public class ImdbControllerIT extends ParentIT {

    @Autowired
    private TestRestTemplate testRestTemplate;

    @Before
    public void setUp(){
        initialize();
        initializeForDegree();
    }

    @Test
    public void findPerson_whenValidPersonName_theReturnOne() {
        ResponseEntity<List<PersonDto>> actualResponse = testRestTemplate.exchange("/api/imdb/person/Gregory Deutsch", HttpMethod.GET,
                null, new ParameterizedTypeReference<List<PersonDto>>(){});

        List<PersonDto> response = actualResponse.getBody();

        assertThat(actualResponse.getStatusCode(), equalTo(HttpStatus.valueOf(HttpStatus.OK.value())));
        assertThat(response, notNullValue());
        assertThat(response, hasSize(1));
        assertThat(response.get(0).getPersonId(), equalTo("nm10237589"));
        assertThat(response.get(0).getName(), equalTo("Gregory Deutsch"));
        assertThat(response.get(0).getBirthYear(), equalTo(null));
        assertThat(response.get(0).getDeathYear(), equalTo(null));
        assertThat(response.get(0).getProfessions(), hasSize(0));
    }

    @Test
    public void findPerson_whenInvalidPersonName_theReturnNotFound() {
        ResponseEntity<String> actualResponse = testRestTemplate.exchange("/api/imdb/person/invalidPersonName", HttpMethod.GET,
                null, String.class);

        assertThat(actualResponse.getStatusCode(), equalTo(HttpStatus.valueOf(HttpStatus.NOT_FOUND.value())));
    }

    @Test
    public void findTypecast_whenValidPersonId_theReturnTypecastTrue() {
        ResponseEntity<TypecastDto> actualResponse = testRestTemplate.exchange("/api/imdb/typecasting/nm10237589", HttpMethod.GET,
                null, TypecastDto.class);

        TypecastDto response = actualResponse.getBody();

        assertThat(actualResponse.getStatusCode(), equalTo(HttpStatus.valueOf(HttpStatus.OK.value())));
        assertThat(response, notNullValue());
        assertThat(response.getNameId(), equalTo("nm10237589"));
        assertThat(response.getName(), equalTo("Gregory Deutsch"));
        assertThat(response.isTypecast(), equalTo(true));
        assertThat(response.getMovies(), notNullValue());
        assertThat(response.getMovies(), hasSize(3));

        assertThat(response.getMovies().get(0).getTitleId(), equalTo("tt0168013"));
        assertThat(response.getMovies().get(0).getTitle(), equalTo("The Joe Franklin Show"));
        assertThat(response.getMovies().get(0).getType(), equalTo("tvSeries"));
        assertThat(response.getMovies().get(0).getGenres(), hasSize(1));
        assertThat(response.getMovies().get(0).getGenres().get(0), equalTo("Talk-Show"));

        assertThat(response.getMovies().get(1).getTitleId(), equalTo("tt9233738"));
        assertThat(response.getMovies().get(1).getTitle(), equalTo("Episode dated 10 January 1963"));
        assertThat(response.getMovies().get(1).getType(), equalTo("tvEpisode"));
        assertThat(response.getMovies().get(1).getGenres(), hasSize(0));

        assertThat(response.getMovies().get(2).getTitleId(), equalTo("tt9251660"));
        assertThat(response.getMovies().get(2).getTitle(), equalTo("Joe Franklin's Memory Lane"));
        assertThat(response.getMovies().get(2).getType(), equalTo("tvEpisode"));
        assertThat(response.getMovies().get(2).getGenres(), hasSize(1));
        assertThat(response.getMovies().get(2).getGenres().get(0), equalTo("Talk-Show"));
    }

    @Test
    public void findTypecast_whenInvalidPersonId_theReturnNotFound() {
        ResponseEntity<String> actualResponse = testRestTemplate.exchange("/api/imdb/typecasting/invalidPersonId", HttpMethod.GET,
                null, String.class);

        assertThat(actualResponse.getStatusCode(), equalTo(HttpStatus.valueOf(HttpStatus.NOT_FOUND.value())));
    }

    @Test
    public void findCoincidence_whenValidPersonIds_theReturnListOfMovies() {
        ResponseEntity<List<MovieDto>> actualResponse = testRestTemplate.exchange(
                "/api/imdb/coincidence?personId1=nm10237589&personId2=nm0507691",
                HttpMethod.GET, null, new ParameterizedTypeReference<List<MovieDto>>(){});

        List<MovieDto> response = actualResponse.getBody();

        assertThat(actualResponse.getStatusCode(), equalTo(HttpStatus.valueOf(HttpStatus.OK.value())));
        assertThat(response, notNullValue());
        assertThat(response, hasSize(1));
        assertThat(response.get(0).getTitleId(), equalTo("tt9233738"));
        assertThat(response.get(0).getTitle(), equalTo("Episode dated 10 January 1963"));
        assertThat(response.get(0).getType(), equalTo("tvEpisode"));
        assertThat(response.get(0).getGenres(), hasSize(0));
    }

    @Test
    public void findCoincidence_whenInvalidPersonIds_theReturnEmptyList() {
        ResponseEntity<List<MovieDto>> actualResponse = testRestTemplate.exchange(
                "/api/imdb/coincidence?personId1=nm10237589&personId2=notValid",
                HttpMethod.GET, null, new ParameterizedTypeReference<List<MovieDto>>(){});

        List<MovieDto> response = actualResponse.getBody();

        assertThat(actualResponse.getStatusCode(), equalTo(HttpStatus.valueOf(HttpStatus.OK.value())));
        assertThat(response, notNullValue());
        assertThat(response, hasSize(0));
    }

    @Test
    public void findDegree_whenValidPersonId_theReturnListWithSize2() {
        ResponseEntity<List<String>> actualResponse = testRestTemplate.exchange(
                "/api/imdb/degrees/nm0001229",
                HttpMethod.GET, null, new ParameterizedTypeReference<List<String>>(){});

        List<String> response = actualResponse.getBody();

        assertThat(actualResponse.getStatusCode(), equalTo(HttpStatus.valueOf(HttpStatus.OK.value())));
        assertThat(response, notNullValue());
        assertThat(response, hasSize(2));
        assertThat(response.get(0), equalTo("Glenn Ford"));
        assertThat(response.get(1), equalTo("Kevin Bacon"));
    }

    @Test
    public void findDegree_whenValidPersonId_theReturnListWithSize3() {
        ResponseEntity<List<String>> actualResponse = testRestTemplate.exchange(
                "/api/imdb/degrees/nm0874308",
                HttpMethod.GET, null, new ParameterizedTypeReference<List<String>>(){});

        List<String> response = actualResponse.getBody();

        assertThat(actualResponse.getStatusCode(), equalTo(HttpStatus.valueOf(HttpStatus.OK.value())));
        assertThat(response, notNullValue());
        assertThat(response, hasSize(3));
        assertThat(response.get(0), equalTo("Dalton Trumbo"));
        assertThat(response.get(1), equalTo("Glenn Ford"));
        assertThat(response.get(2), equalTo("Kevin Bacon"));
    }

    @Test
    public void findDegree_whenInvalidPersonId_theReturnNotFound() {
        ResponseEntity<String> actualResponse = testRestTemplate.exchange(
                "/api/imdb/degrees/inValidId",
                HttpMethod.GET, null, String.class);

        assertThat(actualResponse.getStatusCode(), equalTo(HttpStatus.valueOf(HttpStatus.NOT_FOUND.value())));
    }

}
